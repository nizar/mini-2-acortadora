# Register your models here.
from django.contrib import admin
from .models import Urls, Counter

admin.site.register(Urls)
admin.site.register(Counter)

