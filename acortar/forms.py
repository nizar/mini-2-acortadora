from django import forms 

class TextInputForm(forms.Form):
    text_input = forms.CharField(label="Introduce texto")
    short_input = forms.CharField(label="Texto acortado", max_length=50, required=False)

