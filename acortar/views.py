from django.db.models import Sum
import string
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .models import Urls, Counter
from .forms import TextInputForm
# Create your views here.

def input_text(request):
    url = Urls.objects.all()
    contar, created = Counter.objects.get_or_create(id = 1)
    result = None
    url_corto = ""
    print(request)
    if request.method == "POST":
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data["text_input"]
            input_corto = form.cleaned_data.get("short_input")
            if ("http://" not in text) and ("https://" not in text):
                text = "https://" + text  # Añadir http al texto
            txtFilter = Urls.objects.filter(originalUrl = text)
            if txtFilter.exists():
                result = render(request, "received_text.html", {"urls": url})
            else:
                print(input_corto)
                if input_corto:
                    if input_corto.isdigit():
                        if int(input_corto) == contar.counter:
                            url_corto = str(contar.counter)
                            contar.counter = int(input_corto)
                            contar.sumar()
                else:
                    url_corto = str(contar.counter)
                    contar.sumar()
                print(url_corto)
                urltoadd = Urls(originalUrl = text, shortenedUrl = url_corto)
                urltoadd.save()
                result = render(request, "received_text.html", {"urls": url})
                return HttpResponse(result)
    else:
        form = TextInputForm()
        result = render(request, "input_text.html", {"form ": form, "urls": url})
    return result

def Imprimir(request):
    urls = Urls.objects.all()
    return render(request, "html", {"urls": urls})


def Urlredirect(request, shortUrlrequest):
    if request.method == "GET":
        urls = Urls.objects.all()
    try:
        url = Urls.objects.get(shortenedUrl = shortUrlrequest)
    except Urls.DoesNotExist:
        # Si no se encuentra la URL asociada al short URL, mostrar un mensaje de error
        print("URL no encontrada")
        raise Http404("URL no encontrada")

    if url.originalUrl == "":
        # Si la URL original está vacía, mostrar un mensaje de error
        print("ERROR")
        raise Http404("URL no encontrada")
    else:
        print("Redireccionando a:", url.originalUrl)
        return HttpResponseRedirect(url.originalUrl)


