from django.urls import path
from . import views


urlpatterns = [
    path('', views.input_text),
    path('<str:shortUrlrequest>', views.Urlredirect),
]

