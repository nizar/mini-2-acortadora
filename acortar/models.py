from django.db import models

class Counter(models.Model):
    counter = models.IntegerField(default=1)

    def sumar(self):
        self.counter += 1
        self.save()

class Urls(models.Model):
    originalUrl = models.CharField(max_length=255, null=True)
    shortenedUrl = models.CharField(max_length=255, null=True)






